# BooTech
Merhaba. Bu küçük dökümanı kullanarak sizlere BooTech ekibinin yaptığı işlerden özet olarak bahsedeceğiz. Bu dökümanı tüm Tech ekibi olarak hazırlıyoruz, aynı şekilde sizden gelen input'ları da buraya her zaman ekleyebiliriz. Amaçladığımız şey hem ofis içerisinde neler yaptığımızdan biraz haberdar olmanız, hem de *imkanlarımızla neler yapabiliriz* konusunda biraz daha olaylara hakim olmanızı sağlamak.

## AdPro – yedek.adpro.io
AdPro halen aktif olarak geliştirmesine devam ettiğimiz AdWords yönetim platformu. Bunun içinde farklı farklı toollar var, hesaplarınızı yönetirken işinize yarayabilir.

### Toollar
- **Array Finder**: İki liste içerisinden birinci listenin içinde ikinci listedeki elemanları tek tek arayıp eşleşenleri listeler.
- **Unique Word Counter**: Verilen metin içerisinde istenilen sayıda kelimeye sahip kelime öbekleri ve bunların metin içinde kaç defa geçtiklerini sayar.
- **Url Status Checker**: Liste olarak verilen URL’lerin statuslerini kontrol eder.
- **Find & Replace**: Verilen “Source List” içeriside “Find List” olarak verilen kelimeleri “Replace List” ile değiştirir.
- **Keyword Multiplier**: BooTools altında çalışan versiyonun ilkel hali, az sayıda keyword için daha hızlı çalışır.
  
### Raporlar
- **14 Week Summary**: 14 haftalık özet bir rapor sunar, haftalık kırılımda Week, Impression gibi 11 metrik gösterir.
- **Search Term Summary**: İlgili hesaba ait search termlerin belirtilen tarih aralığındaki Impression, Click gibi istatistiklerini sunar.
- **Search Term Analysis**: Search termlerin içinde geçen kelimeleri parçalar ve kelime bazında analiz sunar, bu sayede hesaba ait genel fikir edinme imkanı sunar.
- **Quality Score**: Belirtilen tarihlerde hesaba ait quality score’u değişen kelimeleri listeler.
- **Broken URL**: Hesaba ait URL’lerin statuslerini kontrol eder, 404 olan URL’leri listeler.
  
### Optimizasyon
- **Keyword CPC Optimization**: CPC bazında optimizasyon yapar, direk AdWords’e iletir.
- **Budget Optimization**: Budget optimizasyonu yapar, direk AdWords’e iletir.
- **Bid Modifier**: Device, Hour of day ve day of week kırılımlarında önerilen bid modifier değerlerini sunar.
- **Keyword Suggestion**: Hesabın keyword’leri ve search term’lerini kıyaslar, keyword’lerde olmayan search term’leri önerir.
  
## BooAnalytics - Analytics API
*Google Analytics Reporting V4 API*’ı ile artık iletişim kurabiliyoruz. Buna yönelik çalışması için **analytics.boosmart.com** adresi üzerinde çalışan basit bir bağlantı tool’u geliştirdik. Kısa zamanda değişik aksiyonlar almak için kullanabiliyoruz onu. Halihazırda işinize yarayabilecek yegane fonksiyonu direk Excel’e veri almak olacaktır. Size hazırladığımız bir Excel dosyasını kullanarak istediğiniz rapora yönelik bir URL inşa ediyorsunuz, ardından bu URL’i kullanarak rapor hazırlamak istediğiniz Excel dosyasına “Import from Web” yapıyorsunuz ve bu sayede Analytics arayüzü ile uğraşmadan raporu direk Excel’e çekebiliyorsunuz. Şuan Ads Team’den Beril aktif olarak kullanıyor bu methodu. Buna ek olarak kullanışlı göreceğiniz farklı önerilere de açığız, sürekli tekrarladığınızı düşündüğünüz Analytics işlerini de otomatize etmeyi deneyebiliriz.

------------------------------

## Facebook API
Tech Team'den Talha aktif olarak Facebook üzerine geliştirme yapıyor. Şuan Facebook API'ını kullanarak raporlama yapabilmekle beraber aynı zamanda Facebook için chat-bot da oluşturabiliyor durumdayız, buna yönelik çalıştığımız farklı ürünler var. Talha şu sıralar Analytics API için hazırladığımız URL methoduna benzer bir mekanizma hazırlıyor. Tamamladığında yine aynı şekilde Excel'i kullanarak Facebook raporlarınızı direk Excel'e alma şansınız olacak. Aklınıza gelen farklı kullanımlar, sık tekrarladığınız veya daha iyi olabileceğini düşündüğünüz şeyler için önerilerinize ve tartışmaya açığız.

------------------------------

## BooTools - tools.boosmart.com
- **Alphabetizer**: Bu tool verilen kelimeleri veya cümleleri kelime veya satır bazında sıralıyor.
- **Mail Extractor**: Bu tool verdiğiniz herhangi bir yazı içerisinden içinde geçen mailleri ayıklıyor.
- **Word Counter**: Bu tool verdiğiniz string için kelime, cümle, karakter sayısı gibi sayımlar yapıyor.
- **Keyword Multiplier**: Bu tool 5 sütundan oluşuyor ve verdiğiniz kelimelerin seçtiğiniz match type’lara uygun olacak şekilde kombinasyonlarını yaratıyor.
- **HTTP Status Checker**: Bu tool liste olarak verdiğiniz URL’lerin statuslerini döndürüyor.
  
------------------------------

## AdWords Scripts
AdWords'ün tekrarlanan işleri otomatikleştirmek için kullanıma sunduğu AdWords Scripts adı verilen bir script platformu var bildiğiniz üzre. Bu platform üzerinde çalışması üzere JavaScript programlama dilini kullanarak çeşitli script'ler yazabiliyoruz. Daha önce bir iki script hazırladık, bunları hali hazırda AdTeam'den kullananlar var. Aktif kullanım örneklerimizden biri Yenibiris için hazırladığımız, ilan sayısı belli bir sayının altına indiğinde reklamı durduran, durdurulan reklamları da bir Google Sheets dökümanına yazan ve bunu otomatik olarak Yenibiris tarafındaki ilgiliye mailleyen bir script, haftalık bazda çalışıyor. AdWords Script'lerin çalışma frekanslarını da belirleyip zamanlayabildiğiniz için raporlama ve optimizasyon için farklı farklı scriptler kullanarak manuel işlerinize dair yükünüzü azaltabilirsiniz.
  
